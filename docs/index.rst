.. Python PostX documentation master file, created by
   sphinx-quickstart on Tue Mar 30 10:16:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python-PostX documentation
========================================

PostX is a package that provides a simple interface for posting messages to multiple platforms. 
The user has the ability to send the same message to multiple services or to an indivdual service. 
The same message is translated to the appropriate calls for the each platform.

Getting Started
---------------

Installing via pip

.. code-block:: 
   
   pip install python-postx

Quick Start
-----------


.. code-block:: python

   from postx import Broker
   from postx.message import Message

   # Create the broker
   broker = Broker()

   # You can register as many endpoints as you wish of any combination
   broker.register('slack', 'your slack webhook url')
   broker.register('teams', 'your teams webhook url')

   # Make your message
   message = Message()
   message.title('My Awesome Update')
   message.sub_title('This is an awesome update')
   message.text('The update is for all people to see and affects everything')
   message.list_unordered(['item 1', 'item 2'])
   message.link('Read More Here', 'link to more information')

   # Or a shorter way
   message = Message()
   message.title('My Awesome Update') \
      .sub_title('This is an awesome update') \
      .text('The update is for all people to see and affects everything') \
      .list_unordered(['item 1', 'item 2']) \
      .link('Read More Here', 'link to more information')

   # Or even shorter
   message = Message()
   message.title('My Awesome Update') \
      .sub('This is an awesome update') \
      .text('The update is for all people to see and affects everything') \
      .ul(['item 1', 'item 2']) \
      .link('Read More Here', 'link to more information')

   # Send the message to all
   broker.send(message)

   # Send the message to slack only
   broker.send_single_service(message, 'slack')

   # Send the message to a single endpoint
   broker.send_single_link(message, 'your registered endpoint url')


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   postx


Indices and tables
==================

* :ref:`genindex`


