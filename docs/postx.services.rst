postx.services package
======================

postx.services.slack\_service module
------------------------------------

.. automodule:: postx.services.slack_service
   :members:
   :undoc-members:
   :show-inheritance:

postx.services.teams\_service module
------------------------------------

.. automodule:: postx.services.teams_service
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: postx.services
   :members:
   :undoc-members:
   :show-inheritance:
