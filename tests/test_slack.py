import pytest
from postx.services.slack_service import slack
from postx.message import Message    # noqa


@pytest.fixture()
def slack_ins():
    return slack()


@pytest.fixture()
def message():
    return Message()


def test_slack_text_block(slack_ins, message):
    message.text('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_text_block_type(slack_ins, message):
    message.text('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'section'


def test_slack_text_block_text_type(slack_ins, message):
    message.text('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'plain_text'


def test_slack_text_block_text_text(slack_ins, message):
    message.text('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == 'a'


def test_slack_link_block(slack_ins, message):
    message.link('a', 'b')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_link_block_type(slack_ins, message):
    message.link('a', 'b')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'section'


def test_slack_link_block_text_type(slack_ins, message):
    message.link('a', 'b')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'mrkdwn'


def test_slack_link_block_text_text(slack_ins, message):
    message.link('a', 'b')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == '<b|a>'


def test_slack_title_block(slack_ins, message):
    message.title('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_title_block_type(slack_ins, message):
    message.title('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'header'


def test_slack_title_block_text_type(slack_ins, message):
    message.title('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'plain_text'


def test_slack_title_block_text_text(slack_ins, message):
    message.title('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == 'a'


def test_slack_title_block_text_emoji(slack_ins, message):
    message.title('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['emoji'] is True


def test_slack_sub_block(slack_ins, message):
    message.sub('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_sub_block_type(slack_ins, message):
    message.sub('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'section'


def test_slack_sub_block_text_type(slack_ins, message):
    message.sub('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'mrkdwn'


def test_slack_sub_block_text_text(slack_ins, message):
    message.sub('a')
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == '*a*'


def test_slack_ol_block(slack_ins, message):
    message.ol(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_ol_block_type(slack_ins, message):
    message.ol(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'section'


def test_slack_ol_block_text_type(slack_ins, message):
    message.ol(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'mrkdwn'


def test_slack_ol_block_text_text(slack_ins, message):
    message.ol(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == '1. a\n2. b\n'


def test_slack_ul_block(slack_ins, message):
    message.ul(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]


def test_slack_ul_block_type(slack_ins, message):
    message.ul(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['type'] == 'section'


def test_slack_ul_block_text_type(slack_ins, message):
    message.ul(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['type'] == 'mrkdwn'


def test_slack_ul_block_text_text(slack_ins, message):
    message.ul(['a', 'b'])
    data = slack_ins._build_message(message)
    assert data['blocks'][0]['text']['text'] == '\u2022 a\n\u2022 b\n'
