from postx import Broker, DuplicateError    # noqa
import pytest
from postx.message import Message    # noqa


@pytest.fixture()
def broker():
    return Broker()


@pytest.fixture()
def broker2():
    b = Broker()
    b.register('slack', 'a')
    return b


@pytest.fixture()
def message():
    m = Message()
    m.text('a').text('b')
    return m


def test_duplicate_error_message():
    with pytest.raises(DuplicateError):
        raise DuplicateError


def test_broker_default_registrations(broker):
    assert len(broker._registrations) == 0


def test_broker_services_found(broker):
    assert len(broker._services) >= 1


def test_broker_register_add(broker):
    broker.register('slack', 'a')
    assert len(broker._registrations) == 1


def test_broker_register_add_service_validate(broker):
    broker.register('slack', 'a')
    assert broker._registrations[0][0] == 'slack'


def test_broker_register_add_url_validate(broker):
    broker.register('slack', 'a')
    assert broker._registrations[0][1] == 'a'


def test_broker_register_add_valueerror(broker):
    with pytest.raises(ValueError):
        broker.register('abc', 'a')


def test_broker_register_add_duplicateerror(broker2):
    with pytest.raises(DuplicateError):
        broker2.register('slack', 'a')


def test_broker_unregister_remove(broker2):
    broker2.unregister('a')
    assert len(broker2._registrations) == 0


def test_broker_unregister_remove_valueerror(broker2):
    with pytest.raises(ValueError):
        broker2.unregister('b')


def test_broker_get_services(broker):
    assert len(broker.get_services()) >= 2


def test_broker_get_registrations(broker2):
    assert len(broker2.get_registrations()) == 1


def test_message_send_invalid_message(broker):
    with pytest.raises(TypeError):
        broker.send('a')


def test_message_send_single_link_invalid_message(broker):
    with pytest.raises(TypeError):
        broker.send('a', 'b')


def test_message_send_single_service_invalid_message(broker):
    with pytest.raises(TypeError):
        broker.send('a', 'b')
