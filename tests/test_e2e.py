# This test file is only runnable by CI

import os
import pytest
from postx import Broker
from postx.message import Message

try:
    CI_JOB = os.environ.get('CI_PIPELINE_ID')
    CI_JOB_NAME = os.environ.get('CI_JOB_NAME')
    CI_URL = os.environ.get('CI_PIPELINE_URL')
    CI_COMMIT = os.environ.get('CI_COMMIT_SHORT_SHA')
    CI_AUTHOR = os.environ.get('CI_COMMIT_AUTHOR')
    CI_PROJ_LANGS = os.environ.get('CI_PROJECT_REPOSITORY_LANGUAGES').split(',')

    SLACK_URL = os.environ.get('SLACK_TEST_URL')
    TEAMS_URL = os.environ.get('TEAMS_TEST_URL')
except AttributeError:
    pass


@pytest.fixture
def broker():
    return Broker()


@pytest.fixture
def message():
    msg = Message()
    msg.title(f'e2e Test {CI_JOB} - {CI_JOB_NAME}')\
        .sub('This is only a test')\
        .text('This is running in Gitlab CI')\
        .list_unordered([
            f'COMMIT {CI_COMMIT}', f'AUTHOR {CI_AUTHOR}'
        ])\
        .text('Project Langs')\
        .list_ordered(CI_PROJ_LANGS)\
        .link('Here is the pipeline', CI_URL)
    return msg


@pytest.fixture
def full_broker(broker):
    broker.register('slack', SLACK_URL)
    broker.register('teams', TEAMS_URL)
    return broker


@pytest.mark.ci
def test_e2e_send_all(full_broker, message):
    full_broker.send(message)


@pytest.mark.ci
def test_e2e_send_slack(full_broker, message):
    full_broker.send_single_service(message, 'slack')


@pytest.mark.ci
def test_e2e_send_teams(full_broker, message):
    full_broker.send_single_service(message, 'teams')


@pytest.mark.ci
def test_e2e_send_url_slack(full_broker, message):
    full_broker.send_single_link(message, SLACK_URL)


@pytest.mark.ci
def test_e2e_send_url_teams(full_broker, message):
    full_broker.send_single_link(message, TEAMS_URL)
