import pytest
from postx.services.teams_service import teams
from postx.message import Message    # noqa


@pytest.fixture()
def teams_ins():
    return teams()


@pytest.fixture()
def message():
    return Message()


def test_teams_text_block(teams_ins, message):
    message.text('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_text_block_type(teams_ins, message):
    message.text('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_text_block_text_text(teams_ins, message):
    message.text('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['text'] == 'a'


def test_teams_link_block(teams_ins, message):
    message.link('a', 'b')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_link_block_type(teams_ins, message):
    message.link('a', 'b')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_link_block_text_text(teams_ins, message):
    message.link('a', 'b')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['text'] == '[a](b)'


def test_teams_title_block(teams_ins, message):
    message.title('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_title_block_type(teams_ins, message):
    message.title('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_title_block_text_text(teams_ins, message):
    message.title('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['text'] == 'a'


def test_teams_title_block_text_weight(teams_ins, message):
    message.title('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['weight'] == 'bolder'


def test_teams_title_block_text_size(teams_ins, message):
    message.title('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['size'] == 'large'


def test_teams_sub_block(teams_ins, message):
    message.sub('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_sub_block_type(teams_ins, message):
    message.sub('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_sub_block_text_weight(teams_ins, message):
    message.sub('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['weight'] == 'bolder'


def test_teams_sub_block_text_size(teams_ins, message):
    message.sub('a')
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['size'] == 'medium'


def test_teams_ol_block(teams_ins, message):
    message.ol(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_ol_block_type(teams_ins, message):
    message.ol(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_ol_block_text_text(teams_ins, message):
    message.ol(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['text'
                                                        ] == '1. a\r2. b\r'


def test_teams_ul_block(teams_ins, message):
    message.ul(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]


def test_teams_ul_block_type(teams_ins, message):
    message.ul(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['type'] == 'TextBlock'


def test_teams_ul_block_text_text(teams_ins, message):
    message.ul(['a', 'b'])
    data = teams_ins._build_message(message)
    assert data['attachments'][0]['content']['body'][0]['text'] == '- a\r- b\r'
