from postx.message import Message    # noqa
import pytest
from collections import OrderedDict


@pytest.fixture()
def message():
    return Message()


@pytest.fixture()
def message2():
    m = Message()
    m.text('a').text('b')
    return m


def test_message_default_length(message):
    assert len(message.get_message()) == 0


def test_message_counter_length(message):
    assert message._elementTotal == 0


def test_message_add_text_key(message):
    message.text('a')
    assert 'text' in message.message['0']


def test_message_add_text_value(message):
    message.text('a')
    assert 'a' in message.message['0'].values()


def test_message_add_title_key(message):
    message.title('a')
    assert 'title' in message.message['0']


def test_message_add_title_value(message):
    message.title('a')
    assert 'a' in message.message['0'].values()


def test_message_add_subtitle_key(message):
    message.sub_title('a')
    assert 'sub' in message.message['0']


def test_message_add_subtitle_value(message):
    message.sub_title('a')
    assert 'a' in message.message['0'].values()


def test_message_shorthand_add_subtitle_key(message):
    message.sub('a')
    assert 'sub' in message.message['0']


def test_message_shorthand_add_subtitle_value(message):
    message.sub('a')
    assert 'a' in message.message['0'].values()


def test_message_add_link_key(message):
    message.link('a', 'http://google.com')
    assert 'link' in message.message['0']


def test_message_add_link_value_length(message):
    message.link('a', 'http://google.com')
    assert len(message.message['0']['link']) == 2


def test_message_add_link_value_text(message):
    message.link('a', 'http://google.com')
    assert 'a' in message.message['0']['link'][0]


def test_message_add_link_value_link(message):
    message.link('a', 'http://google.com')
    assert 'http://google.com' in message.message['0']['link'][1]


def test_message_remove_item_int(message2):
    message2.remove_part(0)
    assert len(message2.message) == 1


def test_message_remove_item_str(message2):
    message2.remove_part('0')
    assert len(message2.message) == 1


def test_message_remove_item_value(message2):
    message2.remove_part(0)
    with pytest.raises(KeyError):
        message2.message['0']


def test_message_remove_item_KeyError(message):
    with pytest.raises(KeyError):
        message.remove_part(0)


def test_message_shorthand_remove_item_int(message2):
    message2.rm(0)
    assert len(message2.message) == 1


def test_message_shorthand_remove_item_str(message2):
    message2.rm('0')
    assert len(message2.message) == 1


def test_message_shorthand_remove_item_value(message2):
    message2.rm(0)
    with pytest.raises(KeyError):
        message2.message['0']


def test_message_shorthand_remove_item_KeyError(message):
    with pytest.raises(KeyError):
        message.rm(0)


def test_message_add_list_unordered_key(message):
    message.list_unordered(['a'])
    assert 'list-ul' in message.message['0']


def test_message_add_list_unordered_value_length(message):
    message.list_unordered(['a'])
    assert len(message.message['0']['list-ul']) == 1


def test_message_add_list_unordered_value_text(message):
    message.list_unordered(['a'])
    assert 'a' in message.message['0']['list-ul'][0]


def test_message_add_list_unordered_type(message):
    message.list_unordered(['a'])
    assert type(message.message['0']['list-ul']) == list


def test_message_shorthand_add_list_unordered_key(message):
    message.ul(['a'])
    assert 'list-ul' in message.message['0']


def test_message_shorthand_add_list_unordered_value_length(message):
    message.ul(['a'])
    assert len(message.message['0']['list-ul']) == 1


def test_message_shorthand_add_list_unordered_value_text(message):
    message.ul(['a'])
    assert 'a' in message.message['0']['list-ul'][0]


def test_message_shorthand_add_list_unordered_type(message):
    message.ul(['a'])
    assert type(message.message['0']['list-ul']) == list


def test_message_add_list_ordered_key(message):
    message.list_ordered(['a'])
    assert 'list-ol' in message.message['0']


def test_message_add_list_ordered_value_length(message):
    message.list_ordered(['a'])
    assert len(message.message['0']['list-ol']) == 1


def test_message_add_list_ordered_value_text(message):
    message.list_ordered(['a'])
    assert 'a' in message.message['0']['list-ol'][0]


def test_message_add_list_ordered_type(message):
    message.list_ordered(['a'])
    assert type(message.message['0']['list-ol']) == list


def test_message_shorthand_add_list_ordered_key(message):
    message.ol(['a'])
    assert 'list-ol' in message.message['0']


def test_message_shorthand_add_list_ordered_value_length(message):
    message.ol(['a'])
    assert len(message.message['0']['list-ol']) == 1


def test_message_shorthand_add_list_ordered_value_text(message):
    message.ol(['a'])
    assert 'a' in message.message['0']['list-ol'][0]


def test_message_shorthand_add_list_ordered_type(message):
    message.ol(['a'])
    assert type(message.message['0']['list-ol']) == list


def test_message_get_message(message2):
    assert type(message2.get_message()) is OrderedDict


def test_message_shorthand_get_message(message2):
    assert type(message2.get()) is OrderedDict
