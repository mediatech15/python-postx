from collections import OrderedDict


class Message():
    """
    A Message class for building raw messages to send to various services.
    """
    def __init__(self):
        self.message = OrderedDict()
        self._elementTotal = 0

        self.get = self.get_message
        self.sub = self.sub_title
        self.rm = self.remove_part
        self.ol = self.list_ordered
        self.ul = self.list_unordered

    def text(self, msg):
        """
        Add a text element to the end of the message

        :param msg: text to add to the message
        :type msg: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update({str(self._elementTotal): {'text': str(msg)}})
        self._elementTotal += 1
        return self

    def title(self, title):
        """
        Add a title element to the end of the message

        :param title: title to add to the message
        :type title: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.title('test')

        .. note:: Titles may be placed at the end of the message but may be possitioned properly for the services used. Multiple titles may be ignored. Only the first occurance may be used.

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update({str(self._elementTotal): {'title': str(title)}})
        self._elementTotal += 1
        return self

    def link(self, text, link):
        """
        Add a link element to the end of the message

        :param text: link text to show on the message
        :type text: str
        :param link: link to navigate to in the message
        :type link: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.link('Google', 'https://wwww.google.com')

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update(
            {str(self._elementTotal): {
                'link': [str(text), str(link)]
            }}
        )  # yapf: disable
        self._elementTotal += 1
        return self

    def sub_title(self, sub):
        """
        Add a sub-title element to the end of the message

        :param sub: sub-title to add to the message
        :type sub: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.sub_title('test')

        .. note:: Sub-titles may be placed at the end of the message but may be positioned properly for the services used. Multiple sub-titles may be ignored. Only the first occurance may be used.

        .. tip:: :guilabel:`.sub()` is short-hand for :guilabel:`.sub_title()`

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update({str(self._elementTotal): {'sub': str(sub)}})
        self._elementTotal += 1
        return self

    def remove_part(self, idx):
        """
        Remove an element from Message based on index key

        :param idx: index key of the item
        :type idx: str, int

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')
            message.remove_part(0)
            print(len(message.get_message()))

            >> 0

        .. warning:: Indexes start at **0** and *increase* for each element added. Indexes are **not** re-assigned on removal of one. In the above example the next index is **1 not 0**.

        .. tip:: :guilabel:`.rm()` is short-hand for :guilabel:`.remove_part()`

        :return: Message object (self)
        :rtype: Message

        :raises KeyError: The index was not found in the message
        """
        try:
            self.message.pop(str(idx))
        except KeyError:
            raise
        return self

    def list_unordered(self, item):
        """
        Add a unordered list item to a list. The first occurance of the list is where the list will be

        :param item: item to attach to the list given
        :type item: list of str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.list_unordered('test_item')

        .. tip:: :guilabel:`.ul()` is short-hand for :guilabel:`.list_item_unordered()`

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update(
            {str(self._elementTotal): {
                'list-ul': item
            }}
        )  # yapf: disable
        self._elementTotal += 1
        return self

    def list_ordered(self, item):
        """
        Add a ordered list item to a list. The first occurance of the list is where the list will be

        :param item: item to attach to the list given
        :type item: list of str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.list_ordered('test_item')

        .. tip:: :guilabel:`.ol()` is short-hand for :guilabel:`.list_item_ordered()`

        :return: Message object (self)
        :rtype: Message
        """
        self.message.update(
            {str(self._elementTotal): {
                'list-ol': item
            }}
        )  # yapf: disable
        self._elementTotal += 1
        return self

    def get_message(self):
        """
        Returns the raw message content

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')
            print(message.get_message())

            >> OrderedDict([('0', {'text': 'test'})])

        .. tip:: :guilabel:`.get()` is short-hand for :guilabel:`.get_message()`

        :return: Raw Message Content
        :rtype: OrderedDict


        """
        return self.message
