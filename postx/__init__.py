from inspect import getmembers, ismodule
from postx import services
from postx.message import Message
import importlib

VERSION = '1.0.4'


class DuplicateError(Exception):
    """
    Raised when there is a duplicate entry
    """
    def __init__(self, message='There was a duplicate entry'):
        super().__init__(message)


class Broker():
    """
    A class to Broker messages off to a set of services.
    """
    def __init__(self):
        self._registrations = []
        self._services = getmembers(services, ismodule)

    def register(self, service, url):
        """
        Register a new url and service for messages to be recieved at.

        :param service: type of service for the url
        :type service: str
        :param url: url for messages to be sent to
        :type url: str

        .. highlight:: python
        .. code-block:: python

            broker = Broker()
            broker.register('slack', 'http://webhook.url/for/messages')

        .. note:: Registrations are to the Broker instance not global

        :raises DuplicateError: Raised if the url and service already is registered
        :raises ValueError: Raised if the service type is not valid
        """
        if [item for item in self._services if item[0] == service + '_service']:
            if (service, url) not in self._registrations:
                self._registrations.append((service, url))
            else:
                raise DuplicateError('The registration already exists.')
        else:
            raise ValueError('Service not in available services.')

    def unregister(self, url):
        """
        Unregister a url for messages in the Broker.

        :param url: url to be removed
        :type url: str

        .. highlight:: python
        .. code-block:: python

            broker = Broker()
            broker.unregister('http://webhook.url/for/messages')

        :raises ValueError: Raised if the url is not registered
        """
        i = [item for item in self._registrations if item[1] == url]
        if i:
            self._registrations.remove(i[0])
        else:
            raise ValueError('Url not in registered services.')

    def send(self, message):
        """
        Send a message to all the registered services in the Broker

        :param message: Message to send
        :type message: Message

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')
            broker = Broker()
            broker.register('slack', 'http://webhook.url/for/messages')
            broker.send(message)

        :raises TypeError: Raised if message is not a Message object
        """
        if isinstance(message, Message):
            for r in self._registrations:
                service = r[0]
                url = r[1]
                serviceClass = getattr(
                    importlib.
                    import_module('postx.services.' + service + '_service'),
                    service
                )
                s = serviceClass()
                s.send(url, message)
        else:
            raise TypeError(
                f'message is to be an instance of Message but was type:{type(message)}'
            )

    def send_single_service(self, message, service):
        """
        Send a message to all the registered urls in the selected service in the Broker

        :param message: Message to send
        :type message: Message
        :param service: service to send message to
        :type service: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')
            broker = Broker()
            broker.register('slack', 'http://webhook.url/for/messages')
            broker.send_single_service(message, 'slack')

        :raises TypeError: Raised if message is not a Message object
        """
        if isinstance(message, Message):
            i = [r for r in self._registrations if r[0] == service]
            for r in i:
                url = r[1]
                serviceClass = getattr(
                    importlib.
                    import_module('postx.services.' + service + '_service'),
                    service
                )
                s = serviceClass()
                s.send(url, message)
        else:
            raise TypeError(
                f'message is to be an instance of Message but was type:{type(message)}'
            )

    def send_single_link(self, message, url):
        """
        Send a message to the selected url in the Broker

        :param message: Message to send
        :type message: Message
        :param url: url to send message to
        :type url: str

        .. highlight:: python
        .. code-block:: python

            message = Message()
            message.text('test')
            broker = Broker()
            broker.register('slack', 'http://webhook.url/for/messages')
            broker.send_single_link(message, 'http://webhook.url/for/messages')

        :raises TypeError: Raised if message is not a Message object
        """
        if isinstance(message, Message):
            i = [r for r in self._registrations if r[1] == url]
            for r in i:
                service = r[0]
                serviceClass = getattr(
                    importlib.
                    import_module('postx.services.' + service + '_service'),
                    service
                )
                s = serviceClass()
                s.send(url, message)
        else:
            raise TypeError(
                f'message is to be an instance of Message but was type:{type(message)}'
            )

    def get_services(self):
        """
        Returns the services in the package

        .. highlight:: python
        .. code-block:: python

            broker = Broker()
            broker.get_services()

            >> ['slack', 'teams']

        :return: list of services
        :rtype: list
        """
        return [
            x[0].split('_')[0] for x in self._services if x[0] != 'requests'
        ]

    def get_registrations(self):
        """
        Returns the registrations of the Broker

        .. highlight:: python
        .. code-block:: python

            broker = Broker()
            broker.register('slack', 'http://webhook.url/for/messages')
            broker.get_registrations()

            >> [('slack', 'http://webhook.url/for/messages')]

        .. note:: Registrations are to the Broker instance not global

        :return: list of tuples that have the service and url `(service, url)`
        :rtype: list
        """
        return self._registrations
